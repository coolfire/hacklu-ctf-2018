# Hack.lu CTF 2018

## Baby PHP

The challenge points to a url; https://arcade.fluxfingers.net:1819/ which shows some PHP source code

```php
<?php

require_once('flag.php');
error_reporting(0);


if(!isset($_GET['msg'])){
    highlight_file(__FILE__);
    die();
}

@$msg = $_GET['msg'];
if(@file_get_contents($msg)!=="Hello Challenge!"){
    die('Wow so rude!!!!1');
}

echo "Hello Hacker! Have a look around.\n";

@$k1=$_GET['key1'];
@$k2=$_GET['key2'];

$cc = 1337;$bb = 42;

if(intval($k1) !== $cc || $k1 === $cc){
    die("lol no\n");
}

if(strlen($k2) == $bb){
    if(preg_match('/^\d+＄/', $k2) && !is_numeric($k2)){
        if($k2 == $cc){
            @$cc = $_GET['cc'];
        }
    }
}

list($k1,$k2) = [$k2, $k1];

if(substr($cc, $bb) === sha1($cc)){
    foreach ($_GET as $lel => $hack){
        $$lel = $hack;
    }
}

$‮b = "2";$a="‮b";//;1=b

if($$a !== $k1){
    die("lel no\n");
}

// plz die now
assert_options(ASSERT_BAIL, 1);
assert("$bb == $cc");

echo "Good Job ;)";
// TODO
// echo $flag;
```

We can see by the first and last few lines that `flag.php` likely has some code that defines the variable `$flag`, which we assume holds the flag we need.

Also near the bottom we can see there is an `assert()` function call. From experience with PHP malware we happened to know this function interally uses `eval()`, which means we can use it to execute arbitrary php code. (There's a small caveat, but we'll get to that later.)

### Check 1
The first check is easy; `if(!isset($_GET['msg']))` only checks if we have anything set in our `msg` parameter. If we try adding the `msg` parameter like so; `https://arcade.fluxfingers.net:1819/?msg=foo` we see the `Wow so rude!!!!1` message on the page.

### Check 2
Now we need to get past the check `if(@file_get_contents($msg)!=="Hello Challenge!")`. We don't have any information on the files on the system, and we don't know if a file with the contents `Hello Challenge!` even exists so we likely need to feed this data to the function some other way. Luckily the php `file_get_contents()` function accepts much more than just a file on the filesystem. PHP has a whole slew of things called "input filters" which, in spite of what their name suggest don't filter input so much as they just allow you to input stuff directly. In this case we will use the `data:application/x-httpd-php` input filter with a base64 encoding. Then we can just feed the base64-encoded "Hello Challenge!" string directly in the `msg` parameter like so; `https://arcade.fluxfingers.net:1819/?msg=data:application/x-httpd-php;base64,SGVsbG8gQ2hhbGxlbmdlIQ==`.

Note that many other input filters might also be used here to get the same result. For example `https://arcade.fluxfingers.net:1819/?msg=data://text/plain;base64,SGVsbG8gQ2hhbGxlbmdlIQ==` also works just fine.

### Check 3
We now get the message `Hello Hacker! Have a look around. lol no` which we can see in the php source code, is really two messages and we are now being stopped at the check `if(intval($k1) !== $cc || $k1 === $cc)`. Note that both parts of this check are strong compares so the types also need to match. Both parts need to evaluate to `false` to pass this check. Since the code does `$cc = 1337;`, `$cc` is an int in the php code. If we pass the `key1=1337` in our parameters, it will be a string. Conveniently, this is all we need to do to pass both sides of the check. (Remember both sides need to be `false`.) Our full URL is now; `https://arcade.fluxfingers.net:1819/?msg=data:application/x-httpd-php;base64,SGVsbG8gQ2hhbGxlbmdlIQ==&key1=1337`.

### Check 4.1
We now get the output `Hello Hacker! Have a look around. lel no`. Note that it says "lel" and not "lol" anymore, so we have in fact passed the check. We can see the "lel no" string is much further down, but we'll continue with the `if(strlen($k2) == $bb)` check since we will need to pass it so we get the option to overwrite `$cc`, which we will need later.

In this check (`if(strlen($k2) == $bb)`) we are dealing with two new variables, `$bb` we cannot control yet, so we must manipulate `$k2`. This part is simple enough. Since the script does `$bb = 42;` all we need to do is pass a 42 character long string into `key2`.

### Check 4.2
Next we need to make sure this string in `$k2` matches the `if(preg_match('/^\d+＄/', $k2) && !is_numeric($k2))` check. There are again two parts, the first is a regex match and the second says it must not be a numeric value. At first glance this looks like a problem since the regex `/^\d+＄/` looks like we may only have digits in the string, but upon closer inspection the `＄` used is not a regular `$`. This means we this is not a "end of line" regex anchor but rather a litteral charater we can pass in `key2`. Since we now have a dollar sign in our string we will also pass the `!is_numeric($k2)` part of the check. (A dollar sign is not a number after all.)

### Check 4.3
Now we need to pass the last nested check in this block; `if($k2 == $cc)`. At this point `$cc` is still set to the number `1337` so we must craft a 42 character long string that ends has our weird dollarsign in it and is equal to the number `1337`. Luckily this time a loose compare is used (`==` vs. `===`). This means PHP will automatically try to convert our string in `$k2` to a number when comparing it to `$cc`. How nice of PHP! We can accomplish making the string longer by just prefixing zeros untill we have a 42 character long string. Another nice feature of PHP is that it will ignore our `＄` character when trying to convert it to a number. This means we can use `key2=000000000000000000000000000000000001337＄` as our value. This  means our full URL is now; `https://arcade.fluxfingers.net:1819/?msg=data:application/x-httpd-php;base64,SGVsbG8gQ2hhbGxlbmdlIQ==&key1=1337&key2=000000000000000000000000000000000001337%EF%BC%84`. Note that we URL-encoded the odd dollar sign here.

Hooray! We can now control the content of `$cc` from this point forward.

### Side note 1
The line of code `list($k1,$k2) = [$k2, $k1];` just swaps the values of `$k1` and `$k2`.

### Check 5
This next check looks like a real doozy; `if(substr($cc, $bb) === sha1($cc))`. This would mean we need to puts a string in `$cc` which must be a SHA1 sum of itself with the first 42 characters cut off. Finding such a string is probably not a viable option so we will need to find some way to trick PHP into producing the same result for the `substr` and `sha1` functions. As it turns out, both functions produce an error and return `NULL` if we pass them an array! We don't really care about the error in this case, but since we can now make both functions return `NULL` the check passes. The code inside this check allows us to set any variable at all! (This code just sets `$varname` for any `$_GET['varname']`.) Passing an array variable is allowed in PHP, so we're good to go. Our URL is now; `https://arcade.fluxfingers.net:1819/?msg=data:application/x-httpd-php;base64,SGVsbG8gQ2hhbGxlbmdlIQ==&key1=1337&key2=000000000000000000000000000000000001337%EF%BC%84&cc[]=`

### Side note 2
The line of code `$b = "2";$a="b";//;1=b` looks different on the page and may be copy-pasted differently depending on OS/browser/editor combinations since there are right-to-left text control characters inserted into the string. In the browser it will look like `$b=1;//"b"=a$;"2" = b` instead.

### Check 6
We now need to make sure `if($$a !== $k1)` passes so we can get past the last `die()` function call. The `$$a` part may look odd if you have not seen this before but all this means in this case is `$b`. (Since `$a` is 'b', we are really saying `$'b'`. To PHP this just means `$b`.) So all we need to do is make sure `if(2 === $k1)` since we do not want this if-statement to return `true`. Now we can't just use our `key1` parameter since we need that to pass earlier checks, but remember that we gained the ability to overwrite any variable earlier, so we can now directly pass `k1` as a parameter and it will not be overwritten until the values from our `key1` parameter have done their work. This means all we need to do is pass `k1=2`; `https://arcade.fluxfingers.net:1819/?msg=data:application/x-httpd-php;base64,SGVsbG8gQ2hhbGxlbmdlIQ==&key1=1337&key2=000000000000000000000000000000000001337%EF%BC%84&cc[]=&k1=2`.

### Injecting our code into assert()
As we noted earlier `assert()` uses `eval()` internally so we will start by thinking of what code we want to execute. As all we need to do in this case is figure out the content of the `$flag` variable we will pick something simple; `die($flag);`. But remeber when we mentioned there was a caveat with `assert()`? It's time to deal with that. As it happens `assert()` behaves differently when there is an `==` in the asserted code. In that case it will only ever return `true` or `false`, but we also need it to print our output! This means we will have to make sure the `==` is commented out, so our injected php code will have to end in some comment; `die($flag); /*` should do. This is also the reason we cannot use `$cc` to inject our php code, so `$bb` it is. This makes our final URL; `https://arcade.fluxfingers.net:1819/?msg=data:application/x-httpd-php;base64,SGVsbG8gQ2hhbGxlbmdlIQ==&key1=1337&key2=000000000000000000000000000000000001337%EF%BC%84&cc[]=&k1=2&bb=die($flag);/*`

Behold! Our flag is now printed out!
